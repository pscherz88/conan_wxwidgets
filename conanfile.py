from conans import ConanFile, CMake, tools
from os.path import join


class WxwidgetsConan(ConanFile):
    name = "wxwidgets"
    version = "3.1.5"
    license = "wxWindows Library Licence"
    author = "Patrick Scherz patrick.scherz@invaris.com"
    url = ""
    description = "wxWidgets is a C++ library that lets developers create applications for Windows, macOS, Linux and other platforms with a single code base."
    topics = ("gui", "cross platform")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"
    _cmake = None

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def _configure_cmake(self):
        if self._cmake is None:
            cmake = CMake(self)
            cmake.configure(source_folder=self._source_subfolder)
            self._cmake = cmake
        return self._cmake

    def source(self):
        tools.get(
            url="https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.5/wxWidgets-3.1.5.tar.bz2",
        )
        tools.rename("wxWidgets-3.1.5", self._source_subfolder)
        tools.replace_in_file(
            join(self._source_subfolder, "build", "cmake", "init.cmake"),
            "${wxCOMPILER_PREFIX}${wxARCH_SUFFIX}_${lib_suffix}",
            "",
        )
        tools.replace_in_file(
            join(self._source_subfolder, "include", "msvc", "wx", "setup.h"),
            "wxCONCAT6(../../../lib/, wxLIB_SUBDIR, /, wxTOOLKIT_PREFIX, wxSUFFIX, /wx/setup.h)",
            "wxCONCAT4(../../../lib/, wxTOOLKIT_PREFIX, wxSUFFIX, /wx/setup.h)",
        )
        tools.replace_in_file(
            join(self._source_subfolder, "include", "msvc", "wx", "setup.h"),
            "wxCONCAT5(../../../lib/, wxLIB_SUBDIR, /, wxTOOLKIT_PREFIX, /wx/setup.h)",
            "wxCONCAT3(../../../lib/, wxTOOLKIT_PREFIX, /wx/setup.h)",
        )

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
